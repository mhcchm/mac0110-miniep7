# MAC0110 - MiniEP7
# Marcos Henrique Castello - 11443210

function fatorial(n)
    respfat = big(1)
    if n == 0
        return 1
    end
    for j in 1:n
        respfat *= j
    end
    return respfat
end

function elevado(x, y)
    if y == 0
        return 1
    end
    respel = x
    for i in 1:y-1
        respel *= x
    end
    return respel
end

function cos_(x)
    respcos = 0
    for i in 0:10
        numerador = elevado(-1, i)*BigFloat(elevado(x, 2*i))
        denominador = fatorial(2*i)
        respcos += numerador/denominador
    end
    return respcos
end

function sin_(x)
    respsin = 0
    for i in 0:10
        numerador = elevado(-1, i)*BigFloat(elevado(x, 2*i + 1))
        denominador = fatorial(2*i + 1)
        respsin += numerador/denominador
    end 
    return respsin
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan_(x)
    resptan = 0
    for i in 0:10
        numerador = BigInt(elevado(2, 2*i)*(elevado(2, 2*i) - 1))*BigFloat(bernoulli(i))*BigFloat(elevado(x, 2*i-1))
        denominador = fatorial(2*i)
        resptan += numerador/denominador
    end
    return resptan
end    

function quaseigual(v1, v2)
    erro = 0.001
    if abs(v1 - v2) <= erro
        return true
    else
        return false
    end
end

function check_sin(value, x)
    return quaseigual(value, sin(x))
end

function check_cos(value, x)
    return quaseigual(value, cos(x))
end

function check_tan(value, x)
    return quaseigual(value, tan(x))
end

function taylor_cos(x)
    respcos = 0
    for i in 0:10
        numerador = elevado(-1, i)*BigFloat(elevado(x, 2*i))
        denominador = fatorial(2*i)
        respcos += numerador/denominador
    end
    return respcos
end

function taylor_sin(x)
    respsin = 0
    for i in 0:10
        numerador = elevado(-1, i)*BigFloat(elevado(x, 2*i + 1))
        denominador = fatorial(2*i + 1)
        respsin += numerador/denominador
    end 
    return respsin
end

function taylor_tan(x)
    resptan = 0
    for i in 0:10
        numerador = BigInt(elevado(2, 2*i)*(elevado(2, 2*i) - 1))*BigFloat(bernoulli(i))*BigFloat(elevado(x, 2*i-1))
        denominador = fatorial(2*i)
        resptan += numerador/denominador
    end
    return resptan
end   

using Test
function checacos_(x)
    return quaseigual(taylor_cos(x), cos(x))
end
function checasin_(x)
    return quaseigual(taylor_sin(x), sin(x))
end
function checatan_(x)
    return quaseigual(taylor_tan(x), tan(x))
end

function testaparte1()
    @test checacos_(0)
    @test checacos_(pi/6)
    @test checacos_(pi/4)
    @test checacos_(pi/3)
    @test checacos_(pi/2)
    @test checacos_(2*pi/3)
    @test checacos_(3*pi/4)
    @test checacos_(5*pi/6)
    @test checacos_(pi)
    @test checacos_(pi/30)
    @test checacos_(pi/60)
    @test checacos_(3*pi/7)
    @test checacos_(5*pi/7)
    @test checacos_(15*pi/19)
    @test checacos_(27*pi/50)
    @test checasin_(0)
    @test checasin_(pi/6)
    @test checasin_(pi/4)
    @test checasin_(pi/3)
    @test checasin_(pi/2)
    @test checasin_(2*pi/3)
    @test checasin_(3*pi/4)
    @test checasin_(5*pi/6)
    @test checasin_(pi)
    @test checasin_(pi/30)
    @test checasin_(pi/60)
    @test checasin_(3*pi/7)
    @test checasin_(5*pi/7)
    @test checasin_(15*pi/19)
    @test checasin_(27*pi/50)
    @test checatan_(0)
    @test checatan_(pi/6)
    @test checatan_(pi/4)
    @test checacos_(pi/30)
    @test checacos_(pi/60)
    @test checacos_(3*pi/7)
    println("Fim dos testes da parte 1")
    #A precisão da função tan_ da parte 1 está baixa, fazendo 
    #com que checatan_ devolva false a partir de volta de pi/3,
    #por isso não testei valores maiores que esses. 
end

function testaparte2()
    @test check_cos(1, 0)
    @test check_cos(sqrt(3)/2, pi/6)
    @test check_cos(sqrt(2)/2, pi/4)
    @test check_cos(0.5, pi/3)
    @test check_cos(0, pi/2)
    @test check_cos((-1)*0.5, 2*pi/3)
    @test check_cos((-1)*sqrt(2)/2, 3*pi/4)
    @test check_cos((-1)*sqrt(3)/2, 5*pi/6)
    @test check_sin(0, 0)
    @test check_sin(0.5, pi/6)
    @test check_sin(sqrt(2)/2, pi/4)
    @test check_sin(sqrt(3)/2, pi/3)
    @test check_sin(1, pi/2)
    @test check_sin(sqrt(3)/2, 2*pi/3)
    @test check_sin(sqrt(2)/2, 3*pi/4)
    @test check_sin(0.5, 5*pi/6)
    @test check_tan(0, 0)
    @test check_tan(sqrt(3)/3, pi/6)
    @test check_tan(1, pi/4)
    @test check_tan(sqrt(3), pi/3)
    println("Fim dos testes da parte 2")
end

function test()
    testaparte1()
    testaparte2()
end

test()  
